﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Program.cs" company="">
//   
// </copyright>
// <summary>
//   Defines the Program type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Webjob
{
    using System;
    using System.Collections.Generic;
    using System.Linq;


    using Ups.Backend.Adapter;
    using Ups.Backend.Contracts.DataTypes;
    using Ups.Backend.UpdateCore.Persistence.Software;

    public static class Program
    {
        private const string ProLiveConnectionString =
            "Server=ds87-230-83-65.dedicated.hosteurope.de;" + "Database=pro_de;" + "uid=pro_de;"
            + "password=JuL7NayqwTHn;" + "connection timeout=10;";
        
        private const int ChunkSize = 1000;

        #if DEBUG
        private const int MaxChunkSize = 2;
        #endif

        private static readonly Writer StorageWriter    = new Writer(new Storage("software"));
        private static readonly Writer ErrorWriter      = new Writer(new Storage("errorTable"));

        public static void Main()
        {
            Console.Out.Write("\nFetching SoftwareList from Pro.de ...");
            //var softwareList = new Pro().GetAll();
            var softwareList = new Pro(connectionString: ProLiveConnectionString).GetAll();
            Console.Out.WriteLine("done! Found: " + softwareList.Count + " Titles!");

            Console.Out.Write("\nSplitting Softwarelist into chunks(" + ChunkSize + "/chunk) ...");
            var chunks = Chunk(softwareList, ChunkSize);
            Console.Out.WriteLine("done! Total chunk count: " + chunks.Count());
                                       
            var count           = 0;
            foreach (var chunk in chunks)
            {
                #if DEBUG
                if (count >= MaxChunkSize) { break; }
                #endif

                count++;
                Console.Out.WriteLine("\nInserting SoftwareList-Chunk [" + count + "] ...\n");
                var result = StorageWriter.Create(chunk);
                var successInsertCount = result.Values.Count(a => a);
                Console.Out.WriteLine("\ndone! Successful Entries: " + successInsertCount);

                var errorInsertCount = 0;
                if (result.Values.Count(a => !a) > 0)
                {                    
                    var errorResult = result;
                    while (true)
                    {
                        errorResult = ErrorWriter.Create(CreateErrorList(errorResult));
                        errorInsertCount += errorResult.Values.Count(a => a); 
                        if (errorResult.Values.Count(a => !a) == 0) break;
                    }
                    
                    Console.Out.WriteLine("\nError-Entries inserted: " + errorInsertCount); 
                }

                Console.Out.WriteLine("\nTotal Entries inserted: " + (successInsertCount + errorInsertCount));
            }
        }

        private static IEnumerable<Software> CreateErrorList(Dictionary<Software, bool> dict)
        {
            var errorList = new List<Software>();
            foreach (var entry in dict.Where(entry => entry.Value == false))
            {
                entry.Key.Name += "*";
                errorList.Add(entry.Key);
            }

            return errorList;
        }

        private static IEnumerable<IEnumerable<T>> Chunk<T>(this IEnumerable<T> source, int chunksize)
        {
            while (source.Any())
            {
                yield return source.Take(chunksize);
                source = source.Skip(chunksize);
            }
        }
    }
}
